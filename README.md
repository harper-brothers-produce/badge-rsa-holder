# Badge Holder #

We are making an openscad version of the RSA badge holder. This will
make it easier to make one fit to custom size. It will also be useful
to help learn openscad.

# Learning OpenSCAD #

Keep the [cheat sheet](http://www.openscad.org/cheatsheet/index.html) up.

# Token Holder #

I didn't write inscribe. Instead I just offset how wide I wanted the
rim of the token holder. One problem is that the token holder rim is
hardcoded in two places. We should make that a module.

Another problem is the token is going to slip through the token
holder. I need to wrap the token holder. I think I am doing this but
am not sure. We can test this with animations.


## DONE Remove hardcode rim values
## DONE Create `token_holder` module
## DONE figure out width of sides
## DONE Remove wiggle room
## DONE add lanyard cutout
## DONE Remove hardcoded knob values
## DONE `rsa_token_holder` accept vectors
## DONE Remove hardcoded variables
## DONE Implement Badge Holder
## DONE Make badge function
This will make it easier to determine what values are what in holder.scad.
## DONE Make token function
This will make it easier to determine what values are what in holder.scad.

## DONE (measurment) Adjust Badge hole to be higher
## DONE (measurment) token radius bigger
## DONE (code) allow more room for knob
The knob whole needs to extend further to the top of the token holder.
## TODO Learn about thin walls and close proximity prints
This has something to do with how thin the walls are and the trade off
between the nozzle and the how small of movement the head can make.

Figuring this out allows us to make a choice on the thickness of the
holder.
## TODO Print
## TODO Test fit
## TODO Document how to generate badge holder
## TODO Document what to measure
- token

## TODO Animate token going into the holder
There is an example [animation](~/.nix-profile/share/openscad/examples/Advanced/animation.scad) code to give us an idea how it will
work.

## DONE Read about variables in OpenSCAD ##
See open scad user guide on [variable](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/General#Variables).
