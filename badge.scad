//use "rsa_key.scad";
$fn=100;

module render_badge(width=54, height=86, depth=1.25) {
     cube([width, height, depth], center=true);
}

module l_hole_punch(dimensions) {
     // dimensions [hole_width, hole_height, hole_depth]
     // the hole punch will have rounded edges
     number_of_circles = ceil(dimensions.x/dimensions.y);
     step = dimensions.x / number_of_circles;
     start_x = -(dimensions.x / 2) + (dimensions.y/2);
     end_x = (dimensions.x / 2);

     hull() {
	  for (x = [start_x:step:end_x]) {
	       echo((x - start_x)/dimensions.x);
	       translate([x, 0, 0]) color([(x - start_x)/dimensions.x, 0, 0, 1]) {
		    cylinder(h=dimensions.z, d=dimensions.y, center=true);
	       }
	  }
     }
     cylinder(h=dimensions.z, d=dimensions.y, center=true);
}

module badge_cutout(badge, lanyard_hole, rim) {
     union() {
	  face_cutout = [
	       badge.x - (rim.x * 2),
	       badge.y - rim.y,
	       badge.z + rim.z
	  ];
	  lanyard_cutout = [
	       lanyard_hole[1],
	       lanyard_hole[2],
	       badge.z + rim.z
	  ];
	  // the addtion of y.1 and and the translation of y 1 gives
	  // us a nice cutout.
	  translate ([0, 1, 0]) color ("pink") render_badge(badge.x, badge.y + 1, badge.z);
	  translate ([
	       0,
	       (badge.y/2) - (lanyard_hole[0] + (lanyard_cutout[1]/2)),
	       -lanyard_cutout.z/2
	  ]) color("orange") {
	       l_hole_punch(lanyard_cutout);
	  }
	  color ("green")
	       translate ([0, ((badge.y - face_cutout.y)/2) + 1, face_cutout.z / 2])
	       render_badge(face_cutout.x, face_cutout.y + 1, face_cutout.z);
     }
}

module badge_holder(badge, lanyard_hole, rim) {
     translate([0, 0, badge.z/2 + rim.z]) difference() {
	   translate([0, 0, 0]) render_badge(
	       badge.x + (rim.z * 2),
	       badge.y + rim.z,
	       badge.z + (rim.z * 2)
	       );

	  translate([0, 0, 0]) badge_cutout(
	       badge,
	       lanyard_hole,
	       rim);
     }
}

play = 0.25;
badge_width = 54;
badge_height = 86;
badge_depth = 1.25;

*l_hole_punch([0,0,0], [14, 3, 1]);
translate ([0, 0, 0]) badge_holder(
     [badge_width + 0.5, badge_height, badge_depth + 0.25],
     [5, 14, 3],
     [2, 2, 1]
     );


*translate([0, 10, 0]) color("green") render_badge(badge_width, badge_height, badge_depth);



	  
     
