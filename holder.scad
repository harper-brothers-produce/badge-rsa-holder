use <badge.scad>;
use <rsa_key.scad>;

function badge(width, height, depth, hole) = [[width, height, depth], hole];
function badge_hole(height_from_badge_top, width, height) = [
     height_from_badge_top, width, height
];
function rsa_token(head_radius, body_width, height, depth, knob) = [
     head_radius,
     [body_width, height, depth],
     knob
];
function knob(radius, height) = [radius, height];
function rim(lip, bottom_lip, depth) = [lip, bottom_lip, depth];


module holder(badge, rsa_token, rim) {
     token_head_radius = rsa_token[0];
     token = rsa_token[1];
     b = badge[0];
     union() {
	  color("purple") rsa_token_holder(
	       head_radius=token_head_radius,
	       rsa=token,
	       knob=rsa_token[2],
	       rim=[rim.x, rim.z]
	  );


	  difference() {
	       translate([0, (badge[0].y + rim.z)/2,  token.z + rim.z])
		    badge_holder(
			 badge=b,
			 lanyard_hole=badge[1],
			 rim=rim
			 );
	       color("green") translate([0, rim.z, token.z - rim.z ]) render_rsa_token(token_head_radius, token + [0,0, b.z]);
	  }
     }
	  
	  
     
}

holder(
     badge(
         width        = 55,
         height       = 87,
         depth        = 3,
         hole=badge_hole(
             height_from_badge_top   = 5,
             width                   = 14.1,
             height                  = 3.5
     )),
     rsa_token(
          head_radius    = 14.5,
          body_width     = 20.25,
          height         = 58 + 0.25,
          depth          = 10 + 0.025,
          knob=knob(
               radius    = 2 + 0.05,
               height    = 4
     )),
     rim(
          lip            = 3,
          bottom_lip     = (3 * 1.68),
          depth          = 1.68
    )

);

