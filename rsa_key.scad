/* Matrices definitions
*
* token [width, length, height]
* the body of the token without the knob
*
* knob [knob_raidus, knob_height]
* just the knob of the token
*
* rim [rim_width, rim_sides]
* the rim that is to hold the token
*
* rim_width is the width over the face of the token
* right_sides is how thick the material is you want to keep the sides. */


module render_rsa_token(head_radius, rsa) {
     /* The bottom of token centered at (0,0,0) */
     body_length = rsa.y - head_radius;

     union() {
	  translate([-rsa.x / 2, 0,0])cube([rsa.x, body_length, rsa.z]);
	  translate([0, body_length, 0]) cylinder(h = rsa.z, r = head_radius, $fn=100);

     }
}


module rsa_knob(knob) {
     /* The bottom of knob centered at (0,0,0) */
     color ("green") cylinder(h = knob[1]+1, r = knob[0], $fn = 100);
}

module knob_cutout(knob_radius, rsa) {
     hull() {
	  start = -((rsa.z/2) - knob_radius);
	  end = 1; // give a little more wiggle room
	  number_of_circles = ceil((end - start) / knob_radius);
	  step = (end - start) / number_of_circles;
	  for (i=[start:step:end]) {
	       echo("i", i);
	       translate([0, 0, i]) children(0);
	  }
     }
}


module rsa_token_cutout(head_radius, rsa, knob, rim) {
     /* Centered with the base token at (0,0,0). The cutout is the
      * below that. */
     
     // the +1 depth gives a clean face_cutout
     face_cutout = [
	  head_radius - rim.x,
	  [rsa.x - (rim.x*2), rsa.y - (rim.x*2), rsa.z + rim.y + 1]
     ];
     union () {
	  color("purple") render_rsa_token(head_radius, rsa);

	  knob_cutout(knob[0], rsa) {
	       // The addition of 1 to the knob's height and
	       // translate.y of 1 back into the token gives a nice
	       // union.  If we don't do this the cuts are not made
	       // all the way through when differenced.
	       translate ([0, rsa.y-1, rsa.z / 2]) rotate(a=[-90, 0, 0]) rsa_knob(knob + [0,1]);
	  }

	  color("red") translate([0, rim.x, -(rim.y + 1)]) {
	       render_rsa_token(
		    face_cutout[0],
		    face_cutout[1]
		    );
	  }
     }
}


module rsa_token_holder(head_radius, rsa, knob, rim) {
     difference() {
	  render_rsa_token(head_radius + rim.y, [rsa.x + (rim.y*2), rsa.y + (rim.y * 2), rsa.z + rim.y]);
	  translate([0, rim.y, rim.y])rsa_token_cutout(head_radius, [rsa.x, rsa.y, rsa.z +1], knob, rim);

     }
}


head_radius = 12.5;
length = 56;
body_width = 20;
body_height = 10;

knob_radius = 1.5;
knob_height = 3;

rsa_token_holder(
     head_radius,
     [body_width, length, body_height],
     [knob_radius, knob_height],
     [1.5, 1]
     );
     


